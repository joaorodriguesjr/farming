import { createContext, useCallback, useContext, useState } from 'react'
import { createCrop, createOrders, createSlots, Field, Storage } from '@Game'
import Toast from '@Component/Toast'


export const PlantingContext = createContext(null)

export function usePlanting() {
  const context = useContext(PlantingContext)

  if (! context) {
    throw new Error('usePlanting must be used within a PlantingProvider')
  }

  return context
}

const field = new Field(createSlots(9))
const storage = new Storage(100)
storage.add('CROP_WHEAT', 10)
storage.add('CROP_CORN' , 15)
storage.add('CROP_CANE' , 20)
let orders = createOrders(24, 0)

export const PlantingProvider = ({ children }) => {
  const [ refresh, update ] = useState(Date.now())
  const [ error, updateError ] = useState('')

  const onMenuItemClick = useCallback((item) => {
    try {
      if (storage.hasEnough(item))
        field.plant(createCrop(item))

      storage.remove(item)
    } catch(error) {
      updateError(error.message)
    }

    update(Date.now())
  }, [])

  const onFieldSlotClick = useCallback((slot, index) => {
    if (! slot.crop)
      return

    const { type, kind } = slot.crop

    try {
      if (! storage.willOverflow(2))
        slot.harvest()

      storage.add(`${type}_${kind}`, 2)
    } catch (error) {
      updateError(error.message)
    }

    update(Date.now())
  }, [])

  const canDeliver = useCallback((order) => {
    for (const { item, amount } of order.items) {
      if (! storage.hasEnough(item, amount + 1))
        return false
    }

    return true
  }, [])

  const deliver = useCallback((order) => {
    try {
      for (const { item, amount } of order.items) {
        storage.remove(item, amount)
        order.deliver()
        // orders = orders.filter(current => current !== order)
      }
    } catch (error) {
      updateError(error.message)
    }

    update(Date.now())
  }, [])

  const planting = {
    slots: field.slots, storage, orders,
    onMenuItemClick, onFieldSlotClick,
    canDeliver, deliver
  }

  let toast = null
  if (error) toast = <Toast message={error} onHide={ () => updateError('') }></Toast>

  return (
    <PlantingContext.Provider value={ planting }>
      { toast }
      { children }
    </PlantingContext.Provider>
  )
}

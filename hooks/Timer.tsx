import { useCallback, useEffect, useState } from 'react'

/**
 * One secoond in milliseconds.
 */
const ONE_SECOND = 1000

/**
 * Create a timer hook.
 *
 * @param start The start time in milliseconds.
 * @param duration The duration in milliseconds.
 */
export function useTimer(start: number, duration: number) {
  const currentTime = useCallback(() => {
    return (start + duration - Date.now())
  }, [])

  const timeout = useCallback(() => {
    return currentTime() < ONE_SECOND
  }, [])

  const [ time, updateTime ] = useState(currentTime())

  useEffect(() => {
    if (timeout())
      return

    const id = setTimeout(() => {
      updateTime(currentTime())
    }, ONE_SECOND)

    return () => {
      clearTimeout(id)
    }
  }, [ time ])

  return {
    time, timeout,

    get minutes() {
      return Math.floor(time / 1000 / 60)
    },

    get seconds() {
      return Math.floor(time / 1000 % 60)
    }
  }
}

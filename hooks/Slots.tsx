import { useCallback, useState } from 'react'
import { createCrop, createSlots, Field } from '@Game'


const field = new Field(createSlots(9))

export function useSlots() {
  const [ slots, updateSlots ] = useState(field.slots)

  const plant = useCallback((type: string) => {
    field.plant(createCrop(type))
    updateSlots([ ...field.slots ])
  }, [ slots ])

  const harvest = useCallback((slot: number) => {
    slots[slot].harvest()
    updateSlots([ ...field.slots ])
  }, [ slots ])

  return {
    list: slots, plant, harvest,
  }
}

import { useCallback, useState } from 'react'


const initialState = { 'CROP_WHEAT': 5, 'CROP_CORN': 5, 'CROP_CANE': 5 }

export function useItems() {
  const [ items, updateItems ] = useState(initialState)

  const add = useCallback((item: string, amount = 1) => {
    updateItems({ ...items, [item]: (items[item] || 0) + amount })
  }, [ items ])

  const remove = useCallback((item: string, amount = 1) => {
    updateItems({ ...items, [item]: (items[item] || amount) - amount })
  }, [ items ])

  const count = useCallback((): number => {
    let sum = 0

    for (const item in items) {
      sum += items[item]
    }

    return sum
  }, [ items ])

  return { list: items, count, add, remove }
}

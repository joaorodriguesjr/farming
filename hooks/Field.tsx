import { useReducer } from 'react'

enum Type {  }

type State = {}

type Action = { type: Type, payload: any }

function reducer(state: State, action: Action) {
  switch(action.type) {
    default:
      return state
  }
}

const initialState = { count: 0 }

export function useField() {
  const [ state, dispatch ] = useReducer(reducer, initialState)

  return {}
}

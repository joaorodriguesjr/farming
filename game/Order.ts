import { random, array, today } from './utils'

export type item = { item: string, amount:number }

/**
 * Represents an order that can be delivered.
 */
export class Order {
  /**
   * The time the order was created in milliseconds
   */
  public created: number

  /**
   * The time the order expires in milliseconds
   */
  public expires: number

  /**
   * The items in the order
   */
  public items: item[]

  /**
   * The delivery status of the order
   */
  public delivered: boolean

  /**
   *
   * @param created The time the order was created in milliseconds
   * @param expires The time the order expires in milliseconds
   * @param items The items in the order
   */
  constructor(created: number, expires: number, items: item[]) {
    this.created = created
    this.expires = expires
    this.items = items
    this.delivered = false
  }

  /**
   * @returns If the order is expired
   */
  public isExpired(): boolean {
    return this.expires < Date.now()
  }

  /**
   * Marks this order as delivered
   *
   * @throws If the order is expired
   */
  public deliver() {
    if (this.isExpired()) {
      throw new Error('Cannot deliver an expired order')
    }

    this.delivered = true
  }

  /**
   * Raffles an item based on the provided options filtering the last selected
   *
   * @param options The items that can be chosen
   * @param lastSelected The last item that was selected
   * @returns The raffled item
   */
  public static raffleItem(options: string[], lastSelected: string = null): string {
    if (! lastSelected) {
      return options[random(0, options.length - 1)]
    }

    const filtered = options.filter(option => option !== lastSelected)
    return filtered[random(0, filtered.length - 1)]
  }

  /**
   * Creates a new order with random items that don't repeat
   *
   * @param options The items that can be chosen
   * @param maxItems The maximum amount of each item
   * @returns A newly created order
   */
  public static create(options: string[], maxItems: number): Order {
    let lastSelected: string = null

    // An empty array with a random length mapped to an array of random items
    const items = array(random(1, options.length))
      .map(() => {
        const item = lastSelected = this.raffleItem(options, lastSelected)
        return { item, amount: random(5, maxItems) }
      })

    return new Order(Date.now(), today(), items)
  }

  /**
   * Creates multiple orders with random items that don't repeat
   *
   * @param amount The amount of orders to create
   * @param options The items that can be chosen
   * @param maxItems The maximum amount of each item
   * @returns The created orders
   */
  public static createMultiple(amount: number, options: string[], maxItems: number): Order[] {
    return array(amount).map(() => this.create(options, maxItems))
  }
}

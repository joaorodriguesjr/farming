import { Crop } from './Crop'

/**
 * Represents a corn crop.
 */
export class Corn extends Crop {
  /**
   * @returns The item kind.
   */
  public get kind(): string {
    return 'CORN'
  }
}

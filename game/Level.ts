import { Order } from './Order'

export class Level {
  public dificulty: number

  public orders: Order[]

  public constructor(dificulty: number, orders: Order[]) {
    this.dificulty = dificulty
    this.orders = orders
  }

  public static create(dificulty: number, requiredCrops: string[], requiredOrders: number, maxItems: number): Level {
    const orders = Order.createMultiple(requiredOrders, requiredCrops, maxItems)
    return new Level(dificulty, orders)
  }
}

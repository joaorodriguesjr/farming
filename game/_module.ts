/*****************************************************************************/
/*****                      IMPORT THE GAME CLASSES                      *****/
/*****************************************************************************/

import { Crop } from './Crop'

import { Wheat } from './Wheat'
import { Cane  } from './Cane'
import { Corn  } from './Corn'

import { Slot } from './Slot'
import { Item } from './Item'

import { Order } from './Order'
import { Board } from './Board'




/*****************************************************************************/
/*****                     RE EXPORT THE GAME CLASSES                    *****/
/*****************************************************************************/

export { Crop  } from './Crop'

export { Wheat } from './Wheat'
export { Corn  } from './Corn'
export { Cane  } from './Cane'

export { Field } from './Field'
export { Slot  } from './Slot'

export { Storage  } from './Storage'

export { Order } from './Order'
export { Board } from './Board'




/*****************************************************************************/
/*****                       EXPORT HELPER FUNCTIONS                     *****/
/*****************************************************************************/

const ONE_MINUTE = 1000 * 60


export function createSlots(count: number): Slot[] {
  const slots = []

  while (count > 0) {
    slots.push(new Slot())
    count -= 1
  }

  return slots
}


export function createCrop(type: string): Crop {
  let crop: Crop

  switch (type) {
    case 'CROP_WHEAT':
      crop = new Wheat(Date.now(), ONE_MINUTE / 3)
      break
    case 'CROP_CORN':
      crop = new Corn(Date.now(), ONE_MINUTE / 2)
      break
    case 'CROP_CANE':
      crop = new Cane(Date.now(), ONE_MINUTE / 1)
      break
  }

  return crop
}


const available = [
  [ 'CROP_WHEAT', 'CROP_CORN', 'CROP_CANE' ],
]

const dificulty = [ 15, 30, 60, 120, 240 ]

function array(size: number) {
  return new Array(size).fill(null)
}

function random(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

function today(): number {
  const date = new Date()
  date.setUTCHours(23, 59, 59, 999)
  return date.getTime()
}

export function createOrder(level: number): Order {
  let lastSelected = null
  let options = available[level]

  function raffle(): string {
    let min = 0
    let max = options.length - 1

    if (! lastSelected) {
      return lastSelected = options[random(min, max)]
    }

    options = options.filter(item => item !== lastSelected)
    return lastSelected = options[random(min, options.length - 1)]
  }

  const min = 1
  const max = options.length

  const items = array(random(min, max))
    .map(() => {
      const amount = random(5, dificulty[level])
      return { item: raffle(), amount }
    })

  return new Order(Date.now(), today(), items)
}

export function createOrders(amount: number, level: number): Order[] {
  return array(amount).map(() => createOrder(level))
}

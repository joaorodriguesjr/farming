import { Slot } from './Slot'
import { Crop } from './Crop'

/**
 * Represents a farm field.
 */
export class Field {
  /**
   * The field slots.
   */
  public slots: Slot[]

  /**
   * @param slots The fieldslots.
   */
  public constructor(slots: Slot[]) {
    this.slots = slots
  }

  /**
   * @param crop The crop to be planted.
   * @throws If the field is already full.
   */
  public plant(crop: Crop) {
    let planted = false

    for (const slot of this.slots) {
      if (! slot.empty) {
        continue
      }

      slot.plant(crop)
      planted = true
      break
    }

    if (! planted) {
      throw new Error('No slots available')
    }
  }
}

import { Crop } from './Crop'

/**
* Represents a farm field slot.
*/
export class Slot {
  /**
  * The crop in the slot.
  */
  public crop: Crop

  /**
  * @param crop The crop in the slot.
  */
  constructor(crop: Crop = null) {
    this.crop = crop
  }

  /**
  * @returns If the slot is empty.
  */
  public get empty(): boolean {
    return ! this.crop
  }

  /**
  * Plant a cropt in the slot.
  */
  public plant(crop: Crop): void {
    this.crop = crop
  }

  /**
  * Harvest the crop in the slot.
  *
  * @throws If the crop is not ready.
  * @returns The harvested crop.
  */
  public harvest(): Crop {
    if (! this.crop) {
      throw new Error('No Crop to be harvested')
    }

    if (! this.crop.ready) {
      throw new Error('Crop is not ready')
    }

    const crop = this.crop
    delete this.crop
    return crop
  }
}

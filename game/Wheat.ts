import { Crop } from './Crop'

/**
 * Represents a wheat crop.
 */
export class Wheat extends Crop {
  /**
   * @returns The item kind.
   */
  public get kind(): string {
    return 'WHEAT'
  }
}

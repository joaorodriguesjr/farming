import { Order } from './Order'

export class Board {
  public orders: Order[]

  constructor(orders: Order[] = []) {
    this.orders = orders
  }

  public get empty(): boolean {
    return this.orders.length === 0
  }
}

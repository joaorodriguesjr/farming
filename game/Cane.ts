import { Crop } from './Crop'

/**
 * Represents a sugarcane crop.
 */
export class Cane extends Crop {
  /**
   * @returns The item kind.
   */
  public get kind(): string {
    return 'CANE'
  }
}

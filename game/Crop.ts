import { Item } from './Item'

/**
* Represents a crop in the farm field.
*/
export abstract class Crop extends Item {
  /**
  * The moment when the crop was planted in miliseconds.
  */
  public planted: number

  /**
  * The time the crop needs to grow in miliseconds.
  */
  public harvest: number

  /**
  * @param planted The moment when the crop was planted in miliseconds.
  * @param harvest The time the crop needs to grow in miliseconds.
  */
  constructor(planted: number, harvest: number) {
    super()
    this.planted = planted
    this.harvest = harvest
  }

  /**
  * @returns How many milliseconds are left until the crop is ready to be harvested.
  */
  public get time(): number {
    return (this.planted + this.harvest) - Date.now()
  }

  /**
  * @returns If the crop is ready to be harvested.
  */
  public get ready(): boolean {
    return this.time < 1000
  }

  /**
  * @returns The item type.
  */
  public get type(): string {
    return 'CROP'
  }
}

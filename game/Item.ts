export enum Type { CROP = 'CROP', RESOURCE = 'RESOURCE' }
export enum Kind { WHEAT = 'WHEAT', CORN = 'CORN', CANE = 'CANE' }

/**
* Represents a game item.
*/
export abstract class Item {
  /**
  * @returns The item type.
  */
  public abstract get type(): string

  /**
  * @returns The item kind.
  */
  public abstract get kind(): string
}

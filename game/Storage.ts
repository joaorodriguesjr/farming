/**
 * Represents a storage.
 */
 export class Storage {
  /**
   * The storage capacity.
   */
  public capacity: number

  /**
   * The storage content.
   */
  public items: Map<string, number>

  /**
   * @param capacity The storage capacity.
   */
  public constructor(capacity: number) {
    this.capacity = capacity
    this.items = new Map<string, number>()
  }

  /**
   * Adds a crop to the storage.
   *
   * @throws If the storage is full.
   */
  public add(item:  string, amount: number = 1): void {
    if (this.willOverflow(amount)) {
      throw new Error('No storage space')
    }

    const count = this.items.get(item) || 0
    this.items.set(item, count + amount)
  }

  /**
   * Removes an item from the storage.
   *
   * @throws If there is not enough of the item.
   */
  public remove(item: string, amount: number = 1): void {
    const count = this.items.get(item) || 0

    if (count === 0) {
      throw new Error('Not enough items in storage')
    }

    this.items.set(item, count - amount)
  }

  /**
   * @returns The amount of items in the storage.
   */
  public get count(): number {
    let sum = 0
    this.items.forEach((amount) => sum += amount)

    return sum
  }

  /**
   * @returns If the storage will overflow.
   */
  public willOverflow(amount: number = 1): boolean {
    return this.count + amount > this.capacity
  }

  /**
   * @param item The item to be checkd.
   * @param amount The amount of the item.
   *
   * @returns If the storage has enough of the item.
   */
  public hasEnough(item: string, amount: number = 1): boolean {
    return (this.items.get(item) || 0) >= amount
  }

  public itemAmount(item: string): number {
    return this.items.get(item) || 0
  }
}

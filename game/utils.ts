export function array(size: number) {
  return new Array(size).fill(null)
}

export function random(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

export function today(): number {
  const date = new Date()
  date.setUTCHours(23, 59, 59, 999)
  return date.getTime()
}

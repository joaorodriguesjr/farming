import { usePlanting } from '@Context/Planting'
import styles from './Orders.module.scss'
import Link from 'next/link'
import Order from './Order'


export default function Farm() {
  const { orders } = usePlanting()

  const list = orders.map((order, index: number) => {
    if (order.delivered) return false

    return <div key={index} className={styles.order}>
      <Order order={ order }></Order>
    </div>
  })

  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <div className={styles.back}>
          <Link href={'/'}>
            <button>
              <img src="back.svg" alt="back arrow" width="32" height="32"/>
            </button>
          </Link>
        </div>
        <div className={styles.title}>
          <h1>Today's Orders</h1>
        </div>
      </div>

      <div className={styles.orders}>
        <div className={styles.grid}>{ list }</div>
      </div>

      <div className={styles.footer}>
        <Link href={'/planting'}>
          <button>Plant More</button>
        </Link>
      </div>
    </div>
  )
}

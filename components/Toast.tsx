import { useState } from 'react'
import styles from './Toast.module.scss'
import { useTimeout } from '@Hook/Timeout'

export default function Toast({ message, onHide }) {
  const [ messageClass, updateMessageClass ] = useState(styles.message)

  useTimeout(() => updateMessageClass(`${styles.message} ${styles.show}`), 10  )
  useTimeout(() => updateMessageClass(`${styles.message} ${styles.hide}`), 1000)
  useTimeout(onHide, 1500)

  return (
    <div className={styles.container}>
      <div className={messageClass}>
        {message}
      </div>
    </div>
  )
}

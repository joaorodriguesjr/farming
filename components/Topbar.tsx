import Account from './Account'
import Balance from './Balance'

import styles from './Topbar.module.scss'

export default function Topbar() {
  return (
    <div className={styles.container}>
      <Account address={ '0x00...00' } />
      <Balance amount={ '0.0' } />
    </div>
  )
}

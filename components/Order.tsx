import { usePlanting } from '@Context/Planting'
import styles from './Order.module.scss'


export default function Order({ order }) {
  const { canDeliver, deliver, storage } = usePlanting()

  const items = order.items.map((item, index) => (
    <div key={index} className={styles.item}>
      <div>{ item.item.replace('CROP_', '') }</div>
      <div>
        { storage.itemAmount(item.item) }/{ item.amount }
      </div>
    </div>
  ))

  const delivered = order.delivered ? <div className={ styles.flag }>DELIVERED</div> : <></>

  return (
    <div className={styles.container}>
      { delivered }
      <div className={styles.items}>
        { items }
      </div>
      <div className={ styles.deliver }>
        <button disabled={ ! canDeliver(order) || order.delivered } onClick={() => deliver(order) }>Deliver</button>
      </div>
    </div>
  )
}

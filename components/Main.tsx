import Link from 'next/link'
import styles from './Main.module.scss'

export default function Main() {
  return (
    <div className={styles.container}>
      <div className={styles.report}>
        <h2>Today's Progress</h2>

        <table className={styles.table}>
          <thead>
            <tr>
              <th colSpan={2}>Delivered Orders</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>0/20</td>
              <td>$0.00 / $50.00</td>
            </tr>
          </tbody>
        </table>

        <br />

        <div className={styles.navigation}>
          <div className={styles.item}>
            <Link href={'/trading'}>
              <button>Deliver Orders</button>
            </Link>
          </div>

          <div className={styles.item}>
            <Link href={'/planting'}>
              <button>Plant Crops</button>
            </Link>
          </div>
        </div>
      </div>

      <div className={styles.info}>
        <div>Level 0</div>
        <div>$0.00</div>
      </div>

      <div className={styles.upgrades}>
        <h2>Upgrades</h2>
        <table className={styles.table}>
          <thead>
            <tr>
              <th colSpan={2}>Storage Space</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>100</td>
              <td>
                <Link href={'/'}>
                  <button>Upgrade</button>
                </Link>
              </td>
            </tr>
          </tbody>
        </table>

        <br />

        <table className={styles.table}>
          <thead>
            <tr>
              <th colSpan={2}>Farming Fields</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td>
                <Link href={'/'}>
                  <button>Buy More</button>
                </Link>
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <div className={styles.achievements}>
        <h2>Next Achievements</h2>
        <ul>
          <li>
            <div className={styles.achievement}>
              <div>Deliver 15 Orders</div>
              <div>5%</div>
            </div>
          </li>
          <li>
            <div className={styles.achievement}>
              <div>Plant 200 Crops</div>
              <div>12%</div>
            </div>
          </li>
          <li>
            <div className={styles.achievement}>
              <div>Plant 20 Sugar Canes</div>
              <div>2%</div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  )
}

import styles from './Account.module.scss'

export default function Account({ address }) {
  return (
    <div className={styles.container}>{address}</div>
  )
}

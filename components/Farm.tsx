import { PlantingProvider } from '@Context/Planting'

import styles from './Farm.module.scss'
import Storage from './Farming/Storage'
import Field from './Farming/Field'
import Menu from './Farming/Menu'
import Link from 'next/link'


export default function Farm() {
  return (
    <div className={styles.container}>
      <PlantingProvider>
        <div className={styles.trade}>
          <Link href={'/'}>
            <button>
              <img src="back.svg" alt="back arrow" width="32" height="32"/>
            </button>
          </Link>
        </div>
        <Storage></Storage>
        <Field></Field>
        <Menu></Menu>
      </PlantingProvider>
    </div>
  )
}

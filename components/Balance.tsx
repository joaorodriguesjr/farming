import styles from './Balance.module.scss'

export default function Balance({ amount }) {
  return (
    <div className={styles.container}>{amount}</div>
  )
}

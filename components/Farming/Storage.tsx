import { usePlanting } from '@Context/Planting'
import styles from './Storage.module.scss'

export default function Storage() {
  const { storage } = usePlanting()

  return (
    <div className={styles.container}>
      <div>
        <h2>Storage</h2>
      </div>
      <div>
        { storage.count }/{ storage.capacity }
      </div>
    </div>
  )
}

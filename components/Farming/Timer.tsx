import styles from './Timer.module.scss'
import { useEffect } from 'react'
import { useTimer } from '@Hook/Timer'

/**
 * Displays a timer separating minutes and seconds.
 *
 * @param number time in milliseconds
 */
export default function Timer({ start, duration, onTimeout = () => {} }) {
  const timer = useTimer(start, duration)

  useEffect(() => {
    if (timer.timeout()) onTimeout()
  }, [ timer ])

  if (timer.timeout()) return (
    <div className={styles.container}>
      <>✓</>
    </div>
  )

  return (
    <div className={styles.container}>
      <Unit value={timer.minutes} />m <Unit value={timer.seconds} />s
    </div>
  )
}

/**
 * Displays a time unit.
 *
 * @param number value
 */
function Unit({ value }) {
  return <>
    { value < 10 ? `0${value}` : value }
  </>
}

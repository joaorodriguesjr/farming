import styles from './Menu.module.scss'
import { useCallback } from 'react'
import { usePlanting } from '@Context/Planting'

export default function Menu() {
  const planting = usePlanting()

  const handleClick = useCallback((item: string) => {
    planting.onMenuItemClick(item)
  }, [planting])

  const items = []

  for (const [item, amount] of planting.storage.items) {
    items.push(
      <div key={item} className={ styles.item } onClick={ () => planting.onMenuItemClick(item) }>
        <div className={ styles.name }>{ item.replace('CROP_', '') }</div>
        <div className={ styles.amount }>{ amount }</div>
      </div>
    )
  }

  return <div className={styles.container}>
    { items }
  </div>
}

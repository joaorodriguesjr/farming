import styles from './Slot.module.scss'
import Timer from './Timer'

export default function Slot({ crop, onClick }) {
  let content = null

  if (crop) {
    content = <Timer start={ crop.planted } duration={ crop.harvest } />
  }

  return (
    <div className={styles.container} onClick={onClick}>
      { content }
    </div>
  )
}

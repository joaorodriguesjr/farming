import { usePlanting } from '@Context/Planting'
import styles from './Field.module.scss'
import Slot from './Slot'

export default function Field() {
  const planting = usePlanting()

  const slots = planting.slots.map((slot, index) => {
    return <Slot key={index} crop={slot.crop} onClick={ () => planting.onFieldSlotClick(slot, index) } />
  })

  return (
    <div className={styles.container}>
      { slots }
    </div>
  )
}

import Head from 'next/head'
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {
  return <>
    <Head>
      <meta name="mobile-web-app-capable" content="yes" />
    </Head>
    <Component {...pageProps} />
  </>
}

export default MyApp

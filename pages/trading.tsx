import Orders from '@Component/Orders'
import { PlantingProvider } from '@Context/Planting'

export default function Trade() {
  return (
    <PlantingProvider>
      <Orders />
    </PlantingProvider>
  )
}

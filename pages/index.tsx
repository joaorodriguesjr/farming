import Main from '@Component/Main'
import { PlantingProvider } from '@Context/Planting'

export default function Home() {
  return (
    <PlantingProvider>
      <Main />
    </PlantingProvider>
  )
}
